/*
1. Метод arr.forEach запускає функцію для кожного елемента масиву.
2. Щоб очистити масив потрібно встановити його довжину 0.
3. Можна перевірити, що та чи інша змінна є масивом використавши метод Array.isArray()
*/

function filterBy(array, dataType) {
  const result = array.filter((element) => typeof element !== dataType);
  return result;
}
console.log(filterBy(["hello", "world", 23, "23", null], "string"));
