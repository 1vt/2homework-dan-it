/*
1. Екранування - це коли вставляють спеціальні символи наприклад зворотній слеш
і завдяки цим символам ми даємо зрозуміти що цей текст
повинен трактуватись як звичайні символи.

2. Спосіб оголошення функції:
function name(object) {
  тіло функції;
}
Також є стрілкові функції

3. Hoisting — механізм який перед початком роботи самого коду
пересуває вгору своєї області видимості змінні та оголошення функцій. 
*/


const firstName = prompt("What is your name?");
const lastName = prompt("What is your surname?");

function createNewUser(name, surname) {
  const newUser = {
    firstName: name,
    lastName: surname,
    birthday: '',
    getLogin() {
      return this.firstName.toLowerCase()[0] + this.lastName.toLowerCase();
    },
    getAge() {
      const today = new Date();
      const birthDate = new Date(this.birthday);
      let age = today.getFullYear() - birthDate.getFullYear();
      const monthDay = today.getMonth() - birthDate.getMonth();
      
      if (monthDay < 0 || (monthDay === 0 && today.getDate() < birthDate.getDate())) {
        age--;
      }
      
      return age;
    },
    getPassword() {
      const birthYear = this.birthday.split('.')[2];
      return this.firstName.toUpperCase()[0] + this.lastName.toLowerCase() + birthYear;
    }
  };

  newUser.birthday = prompt("Enter your date of birth (dd.mm.yyyy)");
  return newUser;
}

const newUser = createNewUser(firstName, lastName);
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());