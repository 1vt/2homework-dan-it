/*
1) Функції потрібні щоб зручніше та швидше виконувати деякі задачі.
2) Щоб функція знала з чим працює.
3) За допомогою return повертається результат виконання функції.
*/
const a = +prompt("First number?");
const b = +prompt("Second number?");
const operation = prompt("Your symbol?");

function summ(a, b, operation) {
  switch (operation) {
    case "+":
      return a + b;
    case "-":
      return a - b;
    case "*":
      return a * b;
    case "/":
      if (b !== 0) {
      return  a / b;
      } else {
        return 'Помилка: ділення на нуль';
      }
    default:
      return "Error";
  }
}
console.log(summ(a, b, operation));