/*
1) Змінні можна оголосити через: let, const.  Застарілий варіант: var
2) prompt - користувач може ввести своє значення, confirm - у користувача є два варінти або "ok" або "cancel"
3) Це  перетворення з одного типу в інший наприклад з string в boolean.  
*/

const name = "Vlad";
const admin = name;
console.log(admin);

const days = 3;
const sec = days * 86400
console.log(sec);

const info = prompt("How are you?")
console.log(info);