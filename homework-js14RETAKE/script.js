function toggleTheme() {
  const page = document.querySelector(".page");
  page.classList.toggle("light");
}

if (localStorage.getItem("theme")) {
  const page = document.querySelector(".page");
  page.classList.add(localStorage.getItem("theme"));
}

const changeThemeBtn = document.getElementById("change-theme-btn");
changeThemeBtn.addEventListener("click", () => {
  toggleTheme();

  const page = document.querySelector(".page");
  localStorage.setItem(
    "theme",
    page.classList.contains("light") ? "light" : ""
  );
});
