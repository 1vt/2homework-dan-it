// --- MAIN OUR SERVICE TABS ---
const ourServicesTableTd = document.querySelectorAll(
  ".main-our-services-table-td"
);
console.log(ourServicesTableTd);
const ourServicesTableTdContent = document.querySelectorAll(
  ".main-our-services-table-td-content"
);
console.log(ourServicesTableTdContent);

ourServicesTableTd.forEach(function (item) {
  item.addEventListener("click", function () {
    let currentTab = item;
    let tabId = currentTab.getAttribute("data-tab");
    console.log(tabId);
    let currentContent = document.querySelector(tabId);

    ourServicesTableTd.forEach(function (item) {
      item.classList.remove("active-our-services-td");
    });

    ourServicesTableTdContent.forEach(function (item) {
      item.classList.remove("active-our-services-table-td-content");
    });

    currentTab.classList.add("active-our-services-td");
    currentContent.classList.add("active-our-services-table-td-content");
  });
});

// --- MAIN OUR AMAZING WORK ---
const ourAmazingWork = document.querySelectorAll(".amazing-work-table-tab");
console.log(ourAmazingWork);
const ourAmazingWorkPhoto = document.querySelectorAll(".amazing-grid-tabs");
console.log(ourAmazingWorkPhoto);

ourAmazingWork.forEach(function (itemAmazing) {
  itemAmazing.addEventListener("click", function () {
    let currentTabAmazing = itemAmazing;
    let tabIdAmazing = currentTabAmazing.getAttribute("data-tab");
    console.log(tabIdAmazing);
    let currentContentAmazing = document.querySelector(tabIdAmazing);

    ourAmazingWork.forEach(function (itemAmazing) {
      itemAmazing.classList.remove("active-amazing-work-table-tab");
    });

    ourAmazingWorkPhoto.forEach(function (itemAmazing) {
      itemAmazing.style.display = "none";
    });

    currentTabAmazing.classList.add("active-amazing-work-table-tab");
    currentContentAmazing.style.display = "grid";
  });
});

// --- MAIN LOAD MORE BUTTON ---
const loadMoreButton = document.querySelector(".load-more-button");
const moreBtn = document.querySelector(".more-btn");

moreBtn.addEventListener("click", function () {
  loadMoreButton.insertAdjacentHTML(
    "afterbegin",
    ` <div class="amazing-grid-tabs more-tabs">         <div>
<img
  src="./pictures-ham/graphic-design/graphic-design10.jpg"
  alt="photo"
/>
</div>
<div>
<img src="./pictures-ham/all-amazing/Layer 24.png" alt="photo" />
</div>
<div>
<img src="./pictures-ham/all-amazing/Layer 25.png" alt="photo" />
</div>
<div>
<img src="./pictures-ham/all-amazing/Layer 26.png" alt="photo" />
</div>
<div>
<img src="./pictures-ham/all-amazing/Layer 27.png" alt="photo" />
</div>
<div>
<img src="./pictures-ham/all-amazing/Layer 28.png" alt="photo" />
</div>
<div>
<img src="./pictures-ham/all-amazing/Layer 29.png" alt="photo" />
</div>
<div>
<img src="./pictures-ham/all-amazing/Layer 30.png" alt="photo" />
</div>
<div>
<img src="./pictures-ham/all-amazing/Layer 31.png" alt="photo" />
</div>
<div>
<img src="./pictures-ham/all-amazing/Layer 32.png" alt="photo" />
</div>
<div>
<img src="./pictures-ham/all-amazing/Layer 33.png" alt="photo" />
</div>
<div>
<img src="./pictures-ham/all-amazing/Layer 34.png" alt="photo" />
</div>  </div>`
  );

  moreBtn.style.display = "none";
});

// --- MAIN SLIDER TABS ---

const person = document.querySelectorAll(".feedback-person-slider");
console.log(person);
const memberInfo = document.querySelectorAll(".member-info");
console.log(memberInfo);

person.forEach(function (itemPerson) {
  itemPerson.addEventListener("click", function () {
    let currentTabPerson = itemPerson;
    let tabIdPerson = currentTabPerson.getAttribute("data-tab");
    console.log(tabIdPerson);
    let currentContentPerson = document.querySelector(tabIdPerson);

    person.forEach(function (itemPerson) {
      itemPerson.classList.remove("feedback-person-slider-photo-active");
    });

    memberInfo.forEach(function (itemPerson) {
      itemPerson.classList.remove("active-member-info");
    });

    currentTabPerson.classList.add("feedback-person-slider-photo-active");
    currentContentPerson.classList.add("active-member-info");
  });
});

// --- MAIN SLIDER CLICK ---

let swiper = new Swiper(".mySwiper", {
  slidesPerView: 1,
  grabCursor: true,
  loop: true,
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
});
