// const tabs = document.querySelector(".tabs");
const tabsTitle = document.querySelectorAll(".tabs-title");
const tabsItem = document.querySelectorAll(".tabs-item");

tabsTitle.forEach(function (item) {
  item.addEventListener("click", function () {
    let currentTab = item;
    let tabId = currentTab.getAttribute('data-tab')
    let currentContent = document.querySelector(tabId);

    tabsTitle.forEach(function(item) {
      item.classList.remove("active");
    });

    tabsItem.forEach(function(item) {
        item.classList.remove("active");
      });

    currentTab.classList.add("active");
    currentContent.classList.add("active")
  });
});
