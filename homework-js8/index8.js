/*
1. DOM - об'єктна модель документа. DOM створює браузер, 
завдяки ньому ми легко можемо знайти потрібний елемент і взаємодіяти з ним.
2. innerText повертає весь текст, що міститься в елементі, і всі його дочірні елементи,
а от innerHtml повертає весь текст, включаючи теги html, який міститься в елементі.
3. Всього є 6 способів: getElementById, getElementByClassName, getElementByTagName, getElementByName
і наступні два вважаюьбся кращими: querySelectorAll та querySelector.
*/

//1
/*
const allParagraphs = document.getElementsByTagName(`p`);
console.log(allParagraphs);
allParagraphs[0].style.background = "#ff0000";
*/

const allParagraphs = document.getElementsByTagName("p");
for (let i = 0; i < allParagraphs.length; i++) {
  allParagraphs[i].style.background = "#ff0000";
}

//2

const idElement = document.getElementById('optionsList')
console.log(idElement);
console.log(idElement.parentElement)
console.log(idElement.childNodes);
console.log(typeof(idElement.childNodes));

//3

const testParagraph = document.getElementById('testParagraph')
testParagraph.innerText = "This is a paragraph"

//4

const mainHeader = document.querySelector(".main-header")
const childrenHeader = mainHeader.children
for (let i = 0; i < childrenHeader.length; i++) {
  console.log(childrenHeader[i]);
  childrenHeader[0].classList.add("nav-item")
}

//5

const sectionTitle = document.querySelectorAll(".section-title")
for (let i = 0; i < sectionTitle.length; i++) {
  sectionTitle[i].classList.remove("section-title")
}
console.log(sectionTitle);
