/*
1) Тому що можуть юути некоректні спрацювання.
*/

function pressButton(event) {
  const key = event.key.toLowerCase();
  const buttons = document.querySelectorAll(".btn");
  buttons.forEach((button) => {
    if (button.dataset.key === key) {
      button.style.backgroundColor = "blue";
    }
  });
}
window.addEventListener('keydown', pressButton);

function upButton(event) {
    const key = event.key.toLowerCase();
    const buttons = document.querySelectorAll(".btn");
    buttons.forEach((button) => {
      if (button.dataset.key === key) {
        button.style.backgroundColor = "black";
      }
    });
}
window.addEventListener('keyup', upButton);