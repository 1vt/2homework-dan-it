/*
1) Типи даних у Javascript: Boolean, number, string, null, undefined, object.
2) == - це не строге порівняння, порівнює значення. === - строге порівняння порівнює типи.
3) Є арифметичні операти для виконання математичних дій, є оператори присвоєння що дають значення зміним.
*/

const name = prompt("What is your name?");
if (name === null || name.trim() === "") {
  prompt("What is your name?", name);
}


let age = Number(prompt("How old are you?"));
while (Number.isNaN(age) || age === null || name === "" ) {
  age = Number(prompt("How old are you?"));
}

if (age < 18) {
  alert("You are not allowed to visit this website.");
} else if (age >= 18 && age <= 22) {
  if (confirm("Are you sure you want to continue?")) {
    alert(`Hello, ${name}`);
  } else {
    alert("You are not allowed to visit this website.");
  }
} else {
  alert(`Hello, ${name}`);
}
