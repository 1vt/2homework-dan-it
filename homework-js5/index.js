/*
1) Це функція яка належить об'єкту.
2) Будь-який тип даних.
3) Об'єкт завжди зберігає фактичне значення до якого можна звернутися.
*/

const firstName = prompt("What is your name?");
const lastName = prompt("What is your surname?");

function createNewUser(name, surname) {
  const newUser = {
    firstName: name,
    lastName: surname,
    getLogin() {
      return this.firstName.toLowerCase()[0] + this.lastName.toLowerCase();
    },
  };
  return newUser
}
const newUser = createNewUser(firstName, lastName);
console.log(newUser.getLogin());
