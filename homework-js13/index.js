/*
1) setTimeout() - Спрацьовує один раз.
   setInterval() - Спрацьовує кілька разів з певним інтервалом.
2) Так, спрацює, тому що затримка буду становити нуль секунд.
3) Щоб не було проблем у роботі сайту.
*/

const images = document.querySelector(".images-wrapper");
const photos = document.querySelectorAll(".image-to-show");
const stopShow = document.querySelector(".stop");
const continueShow = document.querySelector(".continue");
let currentIndex = 0;
let intervalId;
function show() {
  for (let i = 0; i < photos.length; i++) {
    if (i === currentIndex) {
        photos[i].style.display = "block";
    } else {
        photos[i].style.display = "none";
    }
  }
  currentIndex++;
  if (currentIndex === photos.length) {
    currentIndex = 0;
  }
}
function slideShow() {
  intervalId = setInterval(show, 3000);
}
slideShow();
stopShow.addEventListener("click", function stopSlideshow() {
  clearInterval(intervalId);
});
continueShow.addEventListener("click", function resumeSlideshow() {
    slideShow();
});
